# TodoList

Team: Elefant

This project was generated with [Angular CLI](https://github.com/angular/angular-cli).

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

# Aufgabenstellung für die Summerschool 2024

[Gitlab_CICD_Aufgaben.pdf](Gitlab_CICD_Aufgaben.pdf)

# Weitere Informationen zu CI/CD

- https://www.martinfowler.com/articles/continuousIntegration.html
- https://www.youtube.com/@ContinuousDelivery
